use std::path::PathBuf;

use clap::Parser;

pub use error::{Error, Result};

mod config;
mod error;
mod server;
mod util;

#[derive(Debug, Parser)]
#[command(version, about, author)]
struct Options {
    #[arg(
        short,
        long,
        default_value = "config.toml",
        env = "QONGZI_CLOUD_API_CONFIG"
    )]
    config: PathBuf,
}

#[tokio::main]
async fn main() -> miette::Result<()> {
    setup_logger();

    let options = Options::parse();
    tracing::debug!("Options: {options:#?}");

    let config = config::Config::load(&options.config)?;
    tracing::debug!("Config: {config:#?}");

    Ok(server::run(config).await?)
}

fn setup_logger() {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{filter::LevelFilter, fmt, EnvFilter};

    let fmt_layer = fmt::layer().without_time().with_target(false);

    let filter_layer = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(fmt_layer)
        .init();
}
