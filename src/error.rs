pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum Error {
    // Generic
    #[error("{msg}")]
    Io {
        #[source]
        err: std::io::Error,
        msg: String,
    },

    // Lib-related
    #[error("Could not load config")]
    Config(#[from] figment::Error),
}

impl Error {
    pub fn io(err: std::io::Error, msg: impl Into<String>) -> Self {
        Self::Io {
            err,
            msg: msg.into(),
        }
    }
}
