use std::net::SocketAddr;
use std::path::Path;

use smart_default::SmartDefault;

use crate::Result;

#[derive(Debug, Clone, SmartDefault, serde::Deserialize, serde::Serialize)]
#[serde(default)]
pub struct Config {
    pub server: ServerConfig,
}

#[derive(Debug, Clone, SmartDefault, serde::Deserialize, serde::Serialize)]
#[serde(default)]
pub struct ServerConfig {
    #[default(ServerConfig::default_listen())]
    pub listen: SocketAddr,
}

impl ServerConfig {
    fn default_listen() -> SocketAddr {
        ([0, 0, 0, 0], 8080).into()
    }
}

impl Config {
    pub fn load(path: &Path) -> Result<Self> {
        use figment::{
            providers::{Env, Format, Toml},
            Figment,
        };

        let figment = Figment::new()
            .merge(Toml::file(path))
            .merge(Env::prefixed("QONGZI_CLOUD_API_").split("_"));

        Ok(figment.extract()?)
    }
}
