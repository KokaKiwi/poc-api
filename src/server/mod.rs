use std::sync::Arc;

use crate::config::Config;
use crate::{Error, Result};

mod routes;

#[derive(Clone)]
pub struct AppState {
    pub config: Arc<Config>,
}

pub async fn run(config: Config) -> Result<()> {
    use tokio::net::TcpListener;

    let listen_addr = config.server.listen;

    let app = routes::router();

    let state = AppState {
        config: Arc::new(config),
    };
    let app = app.with_state(state);

    let listener = TcpListener::bind(&listen_addr)
        .await
        .map_err(|err| Error::io(err, format!("Could not bind TCP socket at {}", listen_addr)))?;

    tracing::info!("Server started at {}", listen_addr);
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown_signal())
        .await
        .map_err(|err| Error::io(err, "Could not serve HTTP app"))
}

async fn shutdown_signal() {
    let _ = tokio::signal::ctrl_c().await;
    tracing::info!("Shutting down...");
}
