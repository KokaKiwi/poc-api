use axum::extract::{Json, Path, State};
use axum::{response::IntoResponse, Router};

use super::AppState;

async fn fallback() -> impl IntoResponse {
    use axum::http::StatusCode;

    (StatusCode::NOT_FOUND, "Not found")
}

async fn home(State(state): State<AppState>) -> impl IntoResponse {
    let config = &*state.config;
    Json(config.clone())
}

async fn list_services() -> impl IntoResponse {
    "list services"
}
async fn create_service(Json(_data): Json<serde_json::Value>) -> impl IntoResponse {
    Json(serde_json::json!({
        "result": "success",
        "id": "fhqsskfjsq"
    }))
}
async fn get_service(Path(id): Path<String>) -> impl IntoResponse {
    format!("Got service {id}")
}

pub fn router() -> Router<AppState> {
    use axum::routing;
    use tower_http::trace;
    use tracing::Level;

    let trace_layer = trace::TraceLayer::new_for_http()
        .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
        .on_request(trace::DefaultOnRequest::new().level(Level::INFO))
        .on_response(trace::DefaultOnResponse::new().level(Level::INFO));

    let app = Router::new();

    let services = Router::new()
        .route("/", routing::get(list_services).post(create_service))
        .route("/:id", routing::get(get_service));

    let app = app
        .route("/", routing::get(home))
        .nest("/service", services);

    app.fallback(fallback).layer(trace_layer)
}
